<!DOCTYPE HTML>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Ali Katiraei | Web Developer, Project Manager, Software Engineer</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Teaching, Coding and live in programming languages, Laravel, PHP, reactjs, js, nodejs, mongo, data analyse, machine learning" />
	<meta name="keywords" content="Web Developement, Laravel Developer, Backend Developer, Nodejs Developer, Fullstack Developer, Nodejs, Laravel, Nextjs, Microservice" />
	<meta name="author" content="Ali Katiraei" />
    <link href="/images/alik-logo.webp" rel="icon">

	

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Space+Mono" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="/css/bootstrap.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="/css/style.css">

	<!-- Modernizr JS -->
	<script src="/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="/js/respond.min.js"></script>
	<![endif]-->
    <link rel="canonical" href="https://www.alikatiraei.com">

	</head>
	<body>
	<script type="application/ld+json"> 
		{
			"@context": "http://www.schema.org",
			"@type": "person",
			"name": "Ali Katiraei",
			"jobTitle": "Fullstack Developer",
			"url": "https://alikatiraei.com",
			"address": {
			"@type": "PostalAddress",
			"streetAddress": "Isfahan, IRAN",
			"addressLocality": "Isfahan",
			"addressRegion": "Isfahan",
			"addressCountry": "IRAN"
			},
			"email": "alikatiraei96@gmail.com",
			"telephone": "+989363147145",
			"birthDate": "1996-08-23"
		}
	</script>

	<div id="myNav" class="overlay">
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	<div class="overlay-content">
		<a onclick="closeNav()" href="#fh5co-header"><div class="profile-thumb"></div></a>
		<a onclick="closeNav()" href="#fh5co-about">About</a>
		<a onclick="closeNav()" href="#fh5co-resume">Experiences</a>
		<a onclick="closeNav()" href="#fh5co-features">Services</a>
		<a onclick="closeNav()" href="#fh5co-skills">Skills</a>
		<a onclick="closeNav()" href="#fh5co-work">Projects</a>
		<a onclick="closeNav()" href="#fh5co-achivements">Achievements</a>
		<a onclick="closeNav()" href="#fh5co-consult">Contact Me</a>
	</div>
	</div>
	<span id="navBtn" onclick="openNav()" class=""><i class="icon-menu"></i></span>
	<div class="fh5co-loader"></div>
	
	<div id="page">	
	<header id="fh5co-header" class="fh5co-cover js-fullheight" role="banner" style="background-image:url(/images/cover_bg_3.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t js-fullheight">
						<div class="display-tc js-fullheight animate-box" data-animate-effect="fadeIn">
							<div class="profile-thumb" style="background: url(/images/user-3.webp);"></div>
							<h1><span>{{$fullName}}</span></h1>
							<h3><span>{{$career}}</span></h3>
							<p>
								<ul class="fh5co-social-icons">
									<li><a href="https://www.linkedin.com/in/ali-katiraei-31b2a61b4/"><i class="icon-linkedin2"></i></a></li>
									<li><a href="mailto:alikatiraei96@gmail.com"><i class="icon-mail"></i></a></li>
									<li><a href="https://t.me/Alik_75"><i class="icon-telegram"></i></a></li>
                                    <li><a href="https://join.skype.com/invite/dO2vNddpS62X"><i class="icon-skype"></i></a></li>
									<li><a href="https://gitlab.com/alik_75"><i class="icon-git"></i></a></li>
									<li><a href="https://stackoverflow.com/users/14882236/ali-katiraie"><i class="icon-stackoverflow"></i></a></li>
									
								</ul>
							</p>
							<p><a href="{{$cvUrl}}" class="btn btn-outline" style="color: white;border-color: white;" download>Download My CV</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-about" class="animate-box">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>About Me</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div style="    height: 400px;">
					<ul class="info">
						<li><span class="first-block">Full Name:</span><span class="second-block">{{$fullName}}</span></li>
						<li><span class="first-block">Phone:</span><span class="second-block">{{$phone}}</span></li>
						<li><span class="first-block">Email:</span><span class="second-block">{{$email}}</span></li>
						<li><span class="first-block">Website:</span><span class="second-block">{{$website}}</span></li>
						{{-- <li><span class="first-block">Address:</span><span class="second-block">198 West 21th Street, Suite 721 New York NY 10016</span></li> --}}
					</ul>
					</div>
					<div style="    height: 400px;">
					<h2 class="info-personal">Personal Abilities</h2>
					<div  class="progress-wrap">
						<h3><span class="name-left">Problem Solver</span><span class="value-right">80%</span></h3>
						<div class="progress">
							<div class="progress-bar progress-bar-1 progress-bar-striped active" role="progressbar"
							aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
							</div>
						</div>
						<p>patience and perseverance and experience use the first solution to the problem</p>
					</div>
					<div  class="progress-wrap">
						<h3><span class="name-left">Want to Learn</span><span class="value-right">90%</span></h3>
						<div class="progress">
							<div class="progress-bar progress-bar-2 progress-bar-striped active" role="progressbar"
							aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
							</div>
						</div>
						<p>it is never too late to learn</p>
					</div>
					<div  class="progress-wrap">
						<h3><span class="name-left">Like Teaching</span><span class="value-right">100%</span></h3>
						<div class="progress">
							<div class="progress-bar progress-bar-3 progress-bar-striped active" role="progressbar"
							aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
							</div>
						</div>
						<p>to raise the level of knowledge of the team</p>
					</div>
					<div  class="progress-wrap">
						<h3><span class="name-left">Team Work</span><span class="value-right">80%</span></h3>
						<div class="progress">
							<div class="progress-bar progress-bar-4 progress-bar-striped active" role="progressbar"
							aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
							</div>
						</div>
						<p>the fastest way to develop a project</p>
					</div>
				</div>
				</div>
				<div class="col-md-8">
					
                        {!! $aboutMe !!}
                    
                    <p>
						
                        <ul class="fh5co-social-icons">
                            <li><a href="https://www.linkedin.com/in/ali-katiraei-31b2a61b4/"><i class="icon-linkedin2"></i></a></li>
                            <li><a href="mailto:alikatiraei96@gmail.com"><i class="icon-mail"></i></a></li>
                            <li><a href="https://t.me/Alik_75"><i class="icon-telegram"></i></a></li>
                            <li><a href="https://join.skype.com/invite/dO2vNddpS62X"><i class="icon-skype"></i></a></li>
                            <li><a href="https://gitlab.com/alik_75"><i class="icon-git"></i></a></li>
                            <li><a href="https://stackoverflow.com/users/14882236/ali-katiraie"><i class="icon-stackoverflow"></i></a></li>
                            
                        </ul>
					</p>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-resume" class="fh5co-bg-color">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>My Resume</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-md-offset-0">
					<ul class="timeline">
						<li class="timeline-heading text-center animate-box">
							<div><h3>Work Experience</h3></div>
						</li>
                        @foreach($experiences as $experience)
						<li class="animate-box @if($loop->iteration%2 != 0 ) timeline-unverted @else timeline-inverted  @endif">
							<div class="timeline-badge"><i class="icon-suitcase"></i></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">{{$experience->title}}</h3>
									<span class="company">{{$experience->company}} <br> {{$experience->start_year}} - {{$experience->end_year}}</span>
								</div>
								<div class="timeline-body">
									<p>{{$experience->description}}</p>
								</div>
							</div>
						</li>
                        @endforeach
						

						<br>
						<li class="timeline-heading text-center animate-box">
							<div><h3>Education</h3></div>
						</li>
                        @foreach($educations as $education)
						<li class="animate-box @if($loop->iteration%2 == 0 ) timeline-unverted @else timeline-inverted  @endif">
							<div class="timeline-badge"><i class="icon-graduation-cap"></i></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">{{$education->degree}}</h3>
									<span class="company">{{$education->university}} <br> {{$education->start_year}} - {{$education->end_year}}</span>
								</div>
								<div class="timeline-body">
									<p>{{$education->description}}</p>
								</div>
							</div>
						</li>
                        @endforeach
						
			    	</ul>
				</div>
			</div>
		</div>
	</div>
	

	<div id="fh5co-features" class="animate-box">
		<div class="container">
			<div class="services-padding">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<h2>My Services</h2>
					</div>
				</div>
                    @foreach($services as $service)
                    @if($loop->iteration%3==1)
                    <div class="row">
                    @endif
					<div class="col-md-4 text-center">
						<div class="feature-left">
							<span class="icon">
								<i class="icon-{{$service->icon}}"></i>
							</span>
							<div class="feature-copy">
								<h3>{{$service->title}}</h3>
								<p>{{$service->description}}</p>
							</div>
						</div>
					</div>
                    @if($loop->iteration%3==0)
                    </div>
                    @endif
                    @endforeach
					
			</div>
		</div>
	</div>

	<div id="fh5co-skills" class="animate-box">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Skills</h2>
				</div>
			</div>
			
			<div class="row">
                @foreach($skillCategories as $category)
                @php
                    $index=rand(1,5);
                @endphp
				<div class="col-md-6">
                    <span class="skill-category-title">{{$category->name}}</span>
                    @foreach($category->skills as $skill)
					<div class="progress-wrap">
						<h3><span class="name-left">{{$skill->title}}</span><span class="value-right">{{$skill->precentage}}%</span></h3>
						<div class="progress">
						  <div class="progress-bar progress-bar-{{($category->id)}} progress-bar-striped active" role="progressbar"
						  aria-valuenow="{{$skill->precentage}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$skill->precentage}}%">
						  </div>
						</div>
					</div>
                    @endforeach
					
				</div>
                @endforeach
				
			</div>
		</div>
	</div>

	

	<div id="fh5co-achivements" class="animate-box">
		<div class="container">
			<div class="services-padding">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<h2>Achievement</h2>
					</div>
				</div>
                    @foreach($achivements as $achivement)
                    @if($loop->iteration%3==1)
                    <div class="row">
                    @endif
					<div class="col-md-4 text-center">
						<div class="feature-left">
							<span class="icon">
								<i class="icon-{{$achivement->icon}}"></i>
							</span>
							<div class="feature-copy">
								<h3>{{$achivement->title}}</h3>
								<p>{{$achivement->description}}</p>
							</div>
						</div>
					</div>
                    @if($loop->iteration%3==0)
                    </div>
                    @endif
                    @endforeach
					
			</div>
		</div>
	</div>

	<div id="fh5co-work">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Projects</h2>
				</div>
			</div>
			<div class="row">
                @foreach($works as $work)
				<div class="col-md-4 text-center col-padding animate-box">
					<a href="{{$work->url}}" target="_blank" class="work" style="background-image: url(storage/{{$work->image}});">
						<div class="desc">
							<h3>{{$work->title}}</h3>
							<span>{{$work->sub_title}}</span>
						</div>
					</a>
				</div>
                @endforeach
				

			</div>
		</div>
	</div>


	<div id="fh5co-started" class="fh5co-bg-dark">
		<div class="overlay"></div>
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Keep in toach!</h2>
					<p>If opportunity doesn't knock, build a door.</p>
					<p><a href="{{$cvUrl}}" class="btn btn-default btn-lg" download>Download My CV</a></p>
				</div>
			</div>
		</div>
	</div>


    <div id="fh5co-consult">
		<div class="video fh5co-video" style="background-image: url(/images/cover_bg_1.jpg);">
			<div class="overlay"></div>
		</div>
		<div class="choose animate-box">
			<h2>Contact</h2>
			<form id="contact-form" action="#">
				<div class="row form-group">
					<div class="col-md-6">
						<input type="text" id="fname" class="form-control" placeholder="Your firstname">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-6">
						<input type="text" id="lname" class="form-control" placeholder="Your lastname">
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12">
						<input type="text" id="email" class="form-control" placeholder="Your email address">
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12">
						<input type="text" id="subject" class="form-control" placeholder="Your subject of this message">
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12">
						<textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Say something about us"></textarea>
					</div>
				</div>
				<div class="form-group">
					<input type="submit" value="Send Message" class="btn btn-primary">
				</div>
				@csrf
			</form>	
		</div>
	</div>
	{{-- <div id="fh5co-blog">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Post on Medium</h2>
					<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#" class="blog-bg" style="background-image: url(/images/portfolio-1.jpg);"></a>
						<div class="blog-text">
							<span class="posted_on">Mar. 15th 2016</span>
							<h3><a href="#">Photoshoot On The Street</a></h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							<ul class="stuff">
								<li><i class="icon-heart2"></i>249</li>
								<li><i class="icon-eye2"></i>308</li>
								<li><a href="#">Read More<i class="icon-arrow-right22"></i></a></li>
							</ul>
						</div> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#" class="blog-bg" style="background-image: url(/images/portfolio-2.jpg);"></a>
						<div class="blog-text">
							<span class="posted_on">Mar. 15th 2016</span>
							<h3><a href="#">Surfing at Philippines</a></h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							<ul class="stuff">
								<li><i class="icon-heart2"></i>249</li>
								<li><i class="icon-eye2"></i>308</li>
								<li><a href="#">Read More<i class="icon-arrow-right22"></i></a></li>
							</ul>
						</div> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#" class="blog-bg" style="background-image: url(/images/portfolio-3.jpg);"></a>
						<div class="blog-text">
							<span class="posted_on">Mar. 15th 2016</span>
							<h3><a href="#">Capture Living On Uderwater</a></h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							<ul class="stuff">
								<li><i class="icon-heart2"></i>249</li>
								<li><i class="icon-eye2"></i>308</li>
								<li><a href="#">Read More<i class="icon-arrow-right22"></i></a></li>
							</ul>
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div> --}}
	
	
	

	
	<div id="fh5co-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>All Rights Reserved. <br>Designed by <a target="_blank">AliK</a> </p>
				</div>
			</div>
		</div>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up22"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="/js/jquery.stellar.min.js"></script>
	<!-- Easy PieChart -->
	<script src="/js/jquery.easypiechart.min.js"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="/js/google_map.js"></script>
	
	<!-- Main -->
	<script src="/js/main.js"></script>
	<script>
		function openNav() {
		  document.getElementById("myNav").style.width = "100%";
		}
		
		function closeNav() {
		  document.getElementById("myNav").style.width = "0%";
		}
		</script>
	</body>
</html>

