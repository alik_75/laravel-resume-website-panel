<?php

namespace App\Http\Controllers;

use App\Models\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function submit(Request $request){
        $firstName=$request->input('firstName');
        $lastName=$request->input('lastName');
        $email=$request->input('email');
        $subject=$request->input('subject');
        $message=$request->input('message');

        $contactUs=new ContactUs();
        $contactUs->name=$firstName." ".$lastName;
        $contactUs->email=$email;
        $contactUs->subject=$subject;
        $contactUs->message=$message;
        $contactUs->save();

        return response()->json([
            "status"=>200,
            "message"=>"Success",
            "data"=>[]
        ]);
    }
}
