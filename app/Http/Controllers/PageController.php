<?php

namespace App\Http\Controllers;

use App\Models\Education;
use App\Models\Experience;
use App\Models\Honor;
use App\Models\Service;
use App\Models\Work;
use Illuminate\Http\Request;
use App\Models\Information;
use App\Models\SkillCategory;

class PageController extends Controller
{
    public function home(){
        $fullName=Information::where("key","fullname")->first();
        $fullName=$fullName?$fullName->value:"";

        $career=Information::where("key","career")->first();
        $career=$career?$career->value:"";

        $aboutMe=Information::where("key","about_me")->first();
        $aboutMe=$aboutMe?$aboutMe->value:"";

        $phone=Information::where("key","phone")->first();
        $phone=$phone?$phone->value:"";

        $email=Information::where("key","email")->first();
        $email=$email?$email->value:"";

        $website=Information::where("key","website")->first();
        $website=$website?$website->value:"";

        $cvUrl=Information::where("key","download_cv")->first();
        $cvUrl=$cvUrl?$cvUrl->value:"";

        $experiences=Experience::orderBy("order_id")->get();

        $educations=Education::orderBy("order_id")->get();

        $services=Service::orderBy("order_id")->get();

        $works=Work::orderBy("order_id")->get();

        $skillCategories=SkillCategory::with('skills')->get();

        $achivements=Honor::orderBy("order_id")->get();
        return view('main',compact('fullName','career','aboutMe','phone','email','website','cvUrl','educations','experiences','services','works','skillCategories','achivements'));
    }
}
